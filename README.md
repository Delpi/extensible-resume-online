# Extensible Résumé Online

## Introduction

This is a simple, extensible web app to generate an online résumé without formatting concerns.

### What can it do

Out of the box, it supports,
* Name, and job title
* Contact data of multiple kinds, with automatic linking
* Organization in categories with in-page navigation built in
* Multiple templates including,
  * Paragraph
  * Paragraph with header
  * Portfolio item
  * Experience item
  
You can combine them in any way you want within any number of categories.

## How to use

### Setting up

The `out` folder holds all the files necessary to building the app, precompiled. The basic setup requires editing the `out/js/profile.json` file with your personal data.

**Note:** This project can not be run locally, unless using a web server. This is because the `profile.json` file is reached with `fetch()`, which is restricted from serving local files for security reasons.

### Customizing the profile

The `profile.json` file has a number of fields, that should be easy to understand for anybody with any Javascript experience. In the interests of convenience, they are detailed as follows:

* **name:** Appears on top of the page by default. Your name.
* **title:** Second line from the top of the page. Your job title, degree, or desired job, as convenient.
* **contact:** An array, bounded by `[]` square brackets, of objects, bounded by `{}` curly brackets. They form the Contact dropdown, and each has,
  * **label:** The name of the field.
  * **content:** Either an explanation, or the url/email/telephone number/value of the field.
  * **link:** (optional) Either leave out, or it ust be "mailto" for emails, or "web" for regular web links.
* **categories:** An array, bounded by `[]` square brackets, of objects, bounded by `{}` curly brackets. It contains the main body of the résumé, as follows:
  * **label:** The title of the category
  * **body:** An array, bounded by `[]` square brackets, of objects, bounded by `{}` curly brackets. The contents of the category, each being a template.
  
### Built in templates

Each template object has, at the very least, fields for
 * **type:** The name of the template being invoked,
 * **content:** An object, bounded by `{}` curly brackets, specific to the template.
 
 The built-in templates, and their usage, is as follows:
 
type | content
---- | -------
p | <ul><li>**p:** A paragraph.</li></ul>
headed-p | <ul><li>**h:** A header.</li><li>**p:** A paragraph.</li></ul>
experience | <ul><li>**h:** A header.</li><li>**start:** (optional) A date of some sort.</li><li>**end:** (optional) A date of some sort.</li><li>**place:** (optional) The name of a place.</li><li>**p:** A paragraph.</li></ul>
portfolio | <ul><li>**h:** A header.</li><li>**date:** (optional) A date of some sort.</li><li>**Notes:** (optional) A short line providing further clarification to the item.</li><li>**link:** (optional) A link to the project explained in the item.</li><li>**p:** A paragraph.</li></ul>

### Modifications

The `src/` folder contains both the sass and jsx files necessary to form this project. They can be edited, recompiled, and outputted to the `out/` folder as needed.

#### New templates

Each template is a React component, and is stored in `src/jsx/templates.jsx`. In turn, the `src/jsx/parser.jsx` file details how the app reads the `profile.json` file and outputs the correct template. Both need to be edited to accomodate for new templates in the project.

