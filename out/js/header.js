export function Header(props) {
    return React.createElement(
        'header',
        null,
        React.createElement(
            'div',
            { className: 'headerTopBar' },
            React.createElement(
                'div',
                { className: 'nameBox' },
                React.createElement(
                    'h1',
                    null,
                    props.name
                )
            )
        ),
        React.createElement(
            'div',
            { className: 'headerBottomBar' },
            React.createElement(
                'div',
                { className: 'titleBox' },
                React.createElement(
                    'h2',
                    null,
                    props.title
                )
            )
        )
    );
}