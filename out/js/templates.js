export function HeadedP(props) {
    return React.createElement(
        'div',
        { className: 'genericTextArea' },
        React.createElement(
            'h4',
            null,
            props.header
        ),
        React.createElement(
            'p',
            null,
            props.p
        )
    );
}

export function Paragraph(props) {
    return React.createElement(
        'div',
        { className: 'genericTextArea' },
        React.createElement(
            'p',
            null,
            props.p
        )
    );
}

export function Experience(props) {
    var Timeframe = function Timeframe(props) {
        var timeString = " ";
        if (props.start) {
            timeString += props.start;
        }
        timeString += " – ";
        if (props.end) {
            timeString += props.end;
        }

        return React.createElement(
            'h5',
            null,
            React.createElement('i', { className: 'fas fa-calendar-alt' }),
            timeString
        );
    };

    var Location = function Location(props) {
        var locationString = ' ' + props.place;
        return React.createElement(
            'h5',
            null,
            React.createElement('i', { className: 'fas fa-map-marker-alt' }),
            locationString
        );
    };

    var PlaceAndTimeBanner = function PlaceAndTimeBanner(props) {
        var timeframe = "",
            place = "";
        if (props.start || props.end) {
            timeframe = React.createElement(Timeframe, { start: props.start,
                end: props.end });
        }
        if (props.place) {
            place = React.createElement(Location, { place: props.place });
        }
        return React.createElement(
            'div',
            { className: 'rowTextArea' },
            timeframe,
            place
        );
    };

    return React.createElement(
        'div',
        { className: 'genericTextArea' },
        React.createElement(
            'h4',
            null,
            props.h
        ),
        React.createElement(PlaceAndTimeBanner, { start: props.start,
            end: props.end,
            place: props.place }),
        React.createElement(
            'p',
            null,
            props.p
        ),
        React.createElement('br', null)
    );
}

export function Portfolio(props) {
    var Timestamp = function Timestamp(props) {
        if (props.date) {
            var dateString = " " + props.date;
            return React.createElement(
                'h5',
                null,
                React.createElement('i', { className: 'fas fa-calendar-alt' }),
                dateString
            );
        } else {
            return "";
        }
    };

    var Link = function Link(props) {
        if (props.href) {
            var linkString = " " + props.href;
            return React.createElement(
                'a',
                { href: props.href },
                React.createElement('i', { className: 'fas fa-link' }),
                linkString
            );
        } else {
            return "";
        }
    };

    return React.createElement(
        'div',
        { className: 'genericTextArea' },
        React.createElement(
            'h4',
            null,
            props.h
        ),
        React.createElement(Timestamp, { date: props.date }),
        React.createElement(
            'h5',
            null,
            props.notes
        ),
        React.createElement(
            'p',
            null,
            props.p
        ),
        React.createElement(Link, { href: props.link }),
        React.createElement('br', null),
        React.createElement('br', null)
    );
}