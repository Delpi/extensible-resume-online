import * as Templates from './templates.js';

function ParseCard(props) {
    var card = props.card;
    switch (card.type) {
        case 'p':
            {
                return React.createElement(Templates.Paragraph, { p: card.content.p });
            }

        case 'headed-p':
            {
                return React.createElement(Templates.HeadedP, { header: card.content.h,
                    p: card.content.p });
            }

        case 'experience':
            {
                return React.createElement(Templates.Experience, { h: card.content.h,
                    start: card.content.start,
                    end: card.content.end,
                    place: card.content.place,
                    p: card.content.p });
            }
        case 'portfolio':
            {
                return React.createElement(Templates.Portfolio, { h: card.content.h,
                    date: card.content.date,
                    notes: card.content.notes,
                    link: card.content.link,
                    p: card.content.p });
            }

        default:
            {
                return React.createElement(Templates.HeadedP, { header: 'Invalid card type!',
                    p: 'The card type ' + card.type + ' is not defined as a valid format' });
            }
    }
}

export function CardBody(props) {
    var body = props.contents.map(function (card) {
        return React.createElement(ParseCard, { card: card });
    });

    return React.createElement(
        'div',
        { className: 'cardContents' },
        body
    );
}