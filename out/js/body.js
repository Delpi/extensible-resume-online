import { Header } from './header.js';
import { Categories } from './categories.js';
import { Navigators } from './navigators.js';

function Body(props) {
    var profile = props.profile;

    return React.createElement(
        'div',
        null,
        React.createElement(Header, { name: profile.name,
            title: profile.title }),
        React.createElement(Categories, { arr: profile.categories }),
        React.createElement('div', { className: 'afterPad' }),
        React.createElement(Navigators, { contacts: profile.contact,
            categories: profile.categories })
    );
}

export function formBody(profile) {
    ReactDOM.render(React.createElement(Body, { profile: profile }), document.getElementById('root'));
}