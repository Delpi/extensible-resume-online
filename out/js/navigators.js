import { Dropdown } from './dropdown.js';

function SearchLabel(props) {
    return React.createElement("i", { className: "fas fa-search" });
}

function ContactLabel(props) {
    return React.createElement("i", { className: "fas fa-address-book" });
}

export function Navigators(props) {
    var categoryNav = props.categories.map(function (category) {
        return {
            href: '#' + category.label.replaceAll(' ', '_'),
            label: category.label
        };
    });

    var contactNav = props.contacts.map(function (contact) {
        var linkString = void 0;
        var labelString = contact.label + ": " + contact.content;
        switch (contact.link) {
            case 'web':
                {
                    linkString = contact.content;
                    break;
                }
            case 'mailto':
                {
                    linkString = "mailto:" + contact.content;
                    break;
                }
            default:
                {
                    linkString = '';
                    break;
                }
        }

        return {
            href: linkString,
            label: labelString
        };
    });

    return React.createElement(
        "navbar",
        null,
        React.createElement(Dropdown, { label: React.createElement(SearchLabel, null),
            children: categoryNav }),
        React.createElement(Dropdown, { label: React.createElement(ContactLabel, null),
            children: contactNav })
    );
}