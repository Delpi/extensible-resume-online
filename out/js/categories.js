import { CardBody } from './parser.js';

function CategoryCard(props) {
    return React.createElement(
        'div',
        { className: 'categoryCard' },
        React.createElement(
            'div',
            { className: 'cardTitleBox' },
            React.createElement(
                'h3',
                { id: props.label.replaceAll(' ', '_') },
                props.label
            )
        ),
        React.createElement(CardBody, { contents: props.body })
    );
}

export function Categories(props) {
    var categoryCards = props.arr.map(function (card) {
        return React.createElement(CategoryCard, { label: card.label,
            body: card.body });
    });
    return React.createElement(
        'div',
        { className: 'categoryList' },
        categoryCards
    );
}