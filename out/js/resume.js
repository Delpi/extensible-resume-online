import { formBody } from './body.js';

async function renderResume () {
    let profile;
    try {
        profile = await fetch('js/profile.json');
        profile = await profile.json();
    } catch (err) {
        alert('Error while fetching data. Please check the console');
        console.log(`Error! Unable to fetch profile.json. Please ensure that,
- The file is located within the js folder,
- You are indeed using a web server to build this, as browsers are unable to fetch from the local filesystem for security reasons,
- The file is syntactically valid.

${err}`);
    }

    formBody(profile);
}

renderResume();
