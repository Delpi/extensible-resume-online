var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

export var Dropdown = function (_React$Component) {
    _inherits(Dropdown, _React$Component);

    function Dropdown(props) {
        _classCallCheck(this, Dropdown);

        var _this = _possibleConstructorReturn(this, (Dropdown.__proto__ || Object.getPrototypeOf(Dropdown)).call(this, props));

        _this.state = {
            visible: false
        };

        _this.children = props.children.map(function (child) {
            return React.createElement(DropdownItem, { label: child.label,
                href: child.href });
        });

        _this.toggleVisible = _this.toggleVisible.bind(_this);
        return _this;
    }

    _createClass(Dropdown, [{
        key: 'toggleVisible',
        value: function toggleVisible() {
            var current = this.state.visible;
            this.setState({
                visible: !current
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var Children = function Children() {
                return _this2.state.visible ? React.createElement(
                    'div',
                    { className: 'dropdown' },
                    _this2.children
                ) : '';
            };

            return React.createElement(
                'div',
                { className: 'dropdownContainer' },
                React.createElement(
                    'div',
                    { className: 'dropButton',
                        onClick: this.toggleVisible },
                    this.props.label
                ),
                React.createElement(Children, null)
            );
        }
    }]);

    return Dropdown;
}(React.Component);

function DropdownItem(props) {
    return React.createElement(
        'a',
        { className: 'dropdownItem',
            href: props.href },
        props.label
    );
}