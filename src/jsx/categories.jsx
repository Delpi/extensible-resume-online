import { CardBody } from './parser.js';

function CategoryCard (props) {
    return (
        <div className='categoryCard'>
            <div className='cardTitleBox'>
                <h3 id={props.label.replaceAll(' ', '_')}>
                    {props.label}</h3>
            </div>
            <CardBody contents={props.body} />
        </div>
    );
}

export function Categories (props) {
    const categoryCards = props.arr.map(
        (card) =>
            <CategoryCard label={card.label}
                          body={card.body} />
    );
    return (
        <div className='categoryList'>
            {categoryCards}
        </div>
    )
}
