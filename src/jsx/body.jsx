import { Header } from './header.js';
import { Categories } from './categories.js';
import { Navigators } from './navigators.js';

function Body (props) {
    const profile = props.profile;

    return (
        <div>
            <Header name={profile.name}
                    title={profile.title} />

            <Categories arr={profile.categories} />
            <div className="afterPad"></div>
            <Navigators contacts={profile.contact}
                        categories={profile.categories} />
        </div>
    );
}

export function formBody (profile) {
    ReactDOM.render(
        <Body profile={profile} />,
        document.getElementById('root')
    );
}
