export function HeadedP (props) {
    return (
        <div className='genericTextArea'>
            <h4>{props.header}</h4>
            <p>{props.p}</p>
        </div>
    );
}

export function Paragraph (props) {
    return (
        <div className='genericTextArea'>
            <p>{props.p}</p>
        </div>
    );
}

export function Experience (props) {
    const Timeframe = (props) => {
        let timeString = " ";
        if (props.start) {
            timeString += props.start;
        }
        timeString += " – ";
        if (props.end) {
            timeString += props.end;
        }

        return (
            <h5>
                <i className="fas fa-calendar-alt"></i>
                {timeString}
            </h5>
        );
    };

    const Location = (props) => {
        const locationString = ` ${props.place}`;
        return (
            <h5>
                <i className="fas fa-map-marker-alt"></i>
                {locationString}
            </h5>
        );
    }

    const PlaceAndTimeBanner = (props) => {
        let timeframe = "",
            place = "";
        if (props.start || props.end) {
            timeframe = <Timeframe start={props.start}
                                   end={props.end} />
        }
        if (props.place) {
            place = <Location place={props.place} />
        }
        return (
            <div className='rowTextArea'>
                {timeframe}
                {place}
            </div>
        );
    }

    return (
        <div className='genericTextArea'>
            <h4>{props.h}</h4>
            <PlaceAndTimeBanner start={props.start}
                                end={props.end}
                                place={props.place} />
            <p>{props.p}</p>
            <br />
        </div>
    );
}

export function Portfolio (props) {
    const Timestamp = (props) => {
        if (props.date) {
            const dateString = " " + props.date;
            return (
                <h5>
                    <i className="fas fa-calendar-alt"></i>
                    {dateString}
                </h5>
            );
        } else {
            return "";
        }
    }

    const Link = (props) => {
        if (props.href) {
            const linkString = " " + props.href;
            return (
                <a href={props.href}>
                    <i className="fas fa-link"></i>
                    {linkString}
                </a>
            );
        } else {
            return "";
        }
    }

    return (
        <div className='genericTextArea'>
            <h4>{props.h}</h4>
            <Timestamp date={props.date} />
            <h5>{props.notes}</h5>
            <p>{props.p}</p>
            <Link href={props.link}/>
            <br />
            <br />
        </div>
    );
}
