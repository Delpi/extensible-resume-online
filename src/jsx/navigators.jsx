import { Dropdown } from './dropdown.js';

function SearchLabel (props) {
    return (
        <i className="fas fa-search"></i>
    );
}

function ContactLabel (props) {
    return (
        <i className="fas fa-address-book"></i>
    );
}

export function Navigators (props) {
    const categoryNav = props.categories.map(
        category => ({
            href: '#' + category.label.replaceAll(' ', '_'),
            label: category.label
        })
    );

    const contactNav = props.contacts.map(
        contact => {
            let linkString;
            const labelString = `${contact.label}: ${contact.content}`
            switch (contact.link) {
                case 'web': {
                    linkString = contact.content;
                    break;
                }
                case 'mailto': {
                    linkString = `mailto:${contact.content}`;
                    break;
                }
                default: {
                    linkString = '';
                    break;
                }
            }

            return {
                href: linkString,
                label: labelString,
            };
        }
    );

    return (
        <navbar>
            <Dropdown label={<SearchLabel />}
                      children={categoryNav} />
            <Dropdown label={<ContactLabel />}
                      children={contactNav} />
        </navbar>
    )
}
