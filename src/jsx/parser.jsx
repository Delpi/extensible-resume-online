import * as Templates from './templates.js';

function ParseCard (props) {
    const card = props.card
    switch (card.type) {
        case 'p': {
            return (
                <Templates.Paragraph p={card.content.p} />
            );
        }

        case 'headed-p': {
            return (
                <Templates.HeadedP header={card.content.h}
                                   p={card.content.p} />
            );
        }

        case 'experience': {
            return (
                <Templates.Experience h={card.content.h}
                                      start={card.content.start}
                                      end={card.content.end}
                                      place={card.content.place}
                                      p={card.content.p} />
            );
        }
        case 'portfolio': {
            return (
                <Templates.Portfolio h={card.content.h}
                                     date={card.content.date}
                                     notes={card.content.notes}
                                     link={card.content.link}
                                     p={card.content.p} />
            );
        }

        default: {
            return (
                <Templates.HeadedP header="Invalid card type!"
                                   p={`The card type ${card.type} is not defined as a valid format`} />
            );
        }
    }
}

export function CardBody (props) {
    const body = props.contents.map(
        (card) =>
            <ParseCard card={card} />
    );

    return (
        <div className='cardContents'>
            {body}
        </div>
    );
}
