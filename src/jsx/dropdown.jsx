export class Dropdown extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            visible: false,
        };

        this.children = props.children.map(
            (child) =>
                <DropdownItem label={child.label}
                              href={child.href}/>
        );

        this.toggleVisible = this.toggleVisible.bind(this);
    }

    toggleVisible () {
        const current = this.state.visible;
        this.setState({
            visible: !current,
        });
    }

    render () {
        const Children = () => {
            return (
                this.state.visible ?
                (
                    <div className='dropdown'>
                        {this.children}
                    </div>
                )
                : ''
            );
        };

        return (
            <div className='dropdownContainer'>
                <div className='dropButton'
                     onClick={this.toggleVisible}>
                    {this.props.label}
                </div>
                <Children />
            </div>
        );
    }
}

function DropdownItem (props) {
    return (
        <a className='dropdownItem'
           href={props.href}>
            {props.label}
        </a>
    );
}
