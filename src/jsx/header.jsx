export function Header (props) {
    return (
        <header>
            <div className='headerTopBar'>
                <div className='nameBox'>
                    <h1>{props.name}</h1>
                </div>
            </div>
            <div className='headerBottomBar'>
                <div className='titleBox'>
                    <h2>{props.title}</h2>
                </div>
            </div>
        </header>
    );
}
